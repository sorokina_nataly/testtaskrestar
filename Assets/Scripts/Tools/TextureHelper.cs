﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Tools
{
    public static class TextureHelper
    {
        public static Texture2D CreateGradientTexture(int width, int height, Color top, Color bottom, Color left, Color right)
        {
            var texture = new Texture2D(width, height, TextureFormat.ARGB32, false);

            float onePerWidth = 1.0f / width;
            float onePerHeight = 1.0f / height;

            for (int y = 0; y < height; ++y)
            {
                var yColor = Color.Lerp(top, bottom, onePerHeight * y);
                for (int x =0; x < width; ++x)
                {
                    var xColor = Color.Lerp(left, right, onePerWidth * x);
                    var color = yColor * xColor;
                    texture.SetPixel(x, y, color);
                }
            }

            texture.Apply();
            return texture;
        }

    }
}
