﻿using System;
using System.IO;
using System.Linq;
using System.Globalization;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Tools
{

    public class ObjFile
    {
        private enum Tag
        {
            Undefined,
            Vertex,
            VertexTexture,
            VertexNormal,
            Face,
        }


        private List<Vector3> vertices;
        private List<Vector3> normals;
        private List<Vector2> uvs;
        private List<VertexIndex[]> faces;

        public bool HasUVs
        {
            get
            {
                return uvs != null;
            }
        }

        private ObjFile()
        {
        }

        public static ObjFile Import(string path)
        {
            var result = new ObjFile();
            result.ReadFile(path);
            return result;
        }



        private struct VertexIndex
        {
            public int vertex;
            public int uv;
            public int normal;

            public VertexIndex(string[] args)
            {
                vertex = int.Parse(args[0]);
                normal = vertex;
                uv = vertex;
                if (args.Length >= 2 && !string.IsNullOrEmpty(args[1]))
                {
                    uv = int.Parse(args[1]);
                }

                if (args.Length >= 3 && !string.IsNullOrEmpty(args[2]))
                {
                    normal = int.Parse(args[2]);
                }
            }
        }

        private MeshTopology TopologyFromVertexCount(int count)
        {
            switch (count)
            {
                case 3:
                    return MeshTopology.Triangles;

                case 4:
                    return MeshTopology.Quads;

            }
            throw new ArgumentException();
        }


        public Mesh GenerateMesh()
        {
            var mesh = new Mesh();

            var uniqIndeces = faces
                .SelectMany(it => it)
                .Distinct()
                .ToList();

            var vxs = uniqIndeces.Select(it => vertices[it.vertex - 1]).ToList();
            var uvx = (uvs.Count == 0 ? null : uniqIndeces.Select(it => uvs[it.uv - 1]).ToList());
            var nms = (normals.Count == 0 ? null : uniqIndeces.Select(it => normals[it.normal - 1]).ToList());

            mesh.SetVertices(vxs);
            if (nms != null)
            {
                mesh.SetNormals(nms);
            }
            if (uvx != null)
            {
                mesh.SetUVs(0, uvx);
            }

            var subMeshes = faces
                .GroupBy(it => it.Length,
                (Key, Collection) => new { Key, Collection })
                .ToList();

            for (int i = 0; i < subMeshes.Count; ++i)
            {
                var topoloigy = TopologyFromVertexCount(subMeshes[i].Key);

                var indeces = subMeshes[i]
                                .Collection
                                .SelectMany(it => it.Select(jt => uniqIndeces.IndexOf(jt)))
                                .ToArray();

                mesh.SetIndices(indeces, topoloigy, i);
            }


            mesh.RecalculateBounds();

            if (nms == null)
            {
                mesh.RecalculateNormals();
            }

            return mesh;
        }

        private void ReadFile(string path)
        {
            faces = new List<VertexIndex[]>();
            vertices = new List<Vector3>();
            normals = new List<Vector3>();
            uvs = new List<Vector2>();

            var lines = File.ReadAllLines(path);
            foreach (var it in lines)
            {
                if (ParseLine(it, out var tag, out var args))
                {
                    switch (tag)
                    {
                        case Tag.Vertex:
                            AddVertex(args);
                            break;

                        case Tag.VertexNormal:
                            AddNormal(args);
                            break;

                        case Tag.VertexTexture:
                            AddUV(args);
                            break;

                        case Tag.Face:
                            AddFace(args);
                            break;
                    }
                }
            }
        }

        private bool ParseLine(string line, out Tag tag, out string[] args)
        {
            tag = Tag.Undefined;
            args = null;

            line = StripComments(line);
            if (!string.IsNullOrEmpty(line))
            {
                var parts = line.Split(new char[] { ' ', '\t' }, StringSplitOptions.RemoveEmptyEntries);

                var tagStr = parts[0].ToLowerInvariant();
                switch (tagStr)
                {
                    case "v":
                        tag = Tag.Vertex;
                        break;

                    case "vt":
                        tag = Tag.VertexTexture;
                        break;

                    case "vn":
                        tag = Tag.VertexTexture;
                        break;

                    case "f":
                        tag = Tag.Face;
                        break;

                    default:
                        return false;
                }
                args = parts.Skip(1).ToArray();

                return true;
            }
            return false;
        }

        private string StripComments(string line)
        {
            var index = line.IndexOf('#');
            if (index >= 0)
            {
                line = line.Substring(0, index);
            }
            return line.Trim();
        }

        private void AddFace(string[] args)
        {
            var face = args
                .Select(it => new VertexIndex(it.Split('/')))
                .ToArray();
            faces.Add(face);
        }

        private void AddUV(string[] args)
        {
            var x = float.Parse(args[0], CultureInfo.InvariantCulture);
            var y = float.Parse(args[1], CultureInfo.InvariantCulture);
            uvs.Add(new Vector2(x, y));
        }

        private void AddNormal(string[] args)
        {
            var x = float.Parse(args[0], CultureInfo.InvariantCulture);
            var y = float.Parse(args[1], CultureInfo.InvariantCulture);
            var z = float.Parse(args[2], CultureInfo.InvariantCulture);
            normals.Add(new Vector3(x, y, z).normalized);
        }

        private void AddVertex(string[] args)
        {
            var x = float.Parse(args[0], CultureInfo.InvariantCulture);
            var y = float.Parse(args[1], CultureInfo.InvariantCulture);
            var z = float.Parse(args[2], CultureInfo.InvariantCulture);
            vertices.Add(new Vector3(x, y, z));
        }
    }
}