﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;
using Tools;

public class CameraGrabber : MonoBehaviour
{
    public event Action<byte[]> OnImageReady = delegate { };

    public void OnPostRender()
    {
        var spriteRect = new Rect(0, 0, Screen.width, Screen.height);
        Texture2D texture = new Texture2D(Screen.width, Screen.height, TextureFormat.ARGB32, false);
        texture.ReadPixels(spriteRect, 0, 0);
        texture.Apply();
        var png = texture.EncodeToPNG();

        OnImageReady?.Invoke(png);
    }
}
