﻿using System;
using System.Collections;
using System.IO;
using UnityEngine;
using Tools;

public class AppMain : MonoBehaviour
{
    [SerializeField]
    private Vector3 rotationAxis;

    [SerializeField]
    private int rotationSteps = 120;

    [SerializeField]
    private MessageBox messageBox;

    [SerializeField]
    private Shader shader;

    private Camera camera;
    private Transform cameraRig;

    private int currentStep = 0;

    private string outputMask;

    private void Start()
    {
        try
        {
#if UNITY_EDITOR
            var models = new string[] { "cube.obj", "torus.obj", "teapot.obj" };
            //var models = new string[] { "unexisted.obj" };
            var modelIndex = UnityEngine.Random.Range(0, int.MaxValue);
            var model = models[modelIndex % models.Length];

            var args = new string[] { "", Path.Combine(Application.dataPath, "..", model), Path.Combine(Application.dataPath, "..",  "RenderResults") };
#else
            var args = Environment.GetCommandLineArgs();
#endif
            var objectPath = args[1];
            var resultPaht = args[2];

            Execute(objectPath, resultPaht);
        }
        catch (Exception e)
        {
            Debug.LogException(e);
            Error(e.Message);
        }
    }


    private void Error(string message)
    {
        messageBox.Show(message,
                () =>
                {
                    Application.Quit(-1);
                },
                null
            );
    }

    private void Update()
    {
        if (cameraRig != null)
        {
            cameraRig.localRotation = Quaternion.Euler(currentStep * rotationAxis / rotationSteps);
            currentStep = (currentStep + 1) % rotationSteps;
        }
    }

    private void Execute(string objectPath, string resultPath)
    {
        camera = CreateCamera();

        var obj = ObjFile.Import(objectPath);
        if (!obj.HasUVs)
        {
            throw new Exception("Model has no UVs");
        }

        void Next()
        {
            outputMask = Path.Combine(resultPath, Path.ChangeExtension(Path.GetFileName(objectPath), "{0}.png"));
            Execute(obj);
        }

        if (!Directory.Exists(resultPath))
        {
            Directory.CreateDirectory(resultPath);
            if (!Directory.Exists(resultPath))
            {
                throw new InvalidOperationException();
            }
            Next();
        }
        else
        {
            messageBox.Show(
                string.Format("Directory \"{0}\" already exists, would you like to overwrite its content?", resultPath),
                Next,
                () => Error("Directory content overwrite has canceled.")
            );
        }
    }

    private void Execute(ObjFile obj)
    {
        var mesh = obj.GenerateMesh();

        var go = new GameObject("ImportedModel");
        var mf = go.AddComponent<MeshFilter>();
        var mr = go.AddComponent<MeshRenderer>();

        mf.sharedMesh = mesh;
        mr.sharedMaterial = CreateMaterial();


        var bounds = mesh.bounds;
        var size = bounds.size;
        var maxDimentions = Mathf.Max(size.x, size.y, size.z);

        cameraRig = SetupCamera(go.transform, maxDimentions);
    }

    private Transform SetupCamera(Transform targetObject, float maxDimentions)
    {
        var grabber = camera.gameObject.AddComponent<CameraGrabber>();
        grabber.OnImageReady += ImageReady;

        var distance = 1.5f * maxDimentions / Mathf.Tan(0.5f * Mathf.Deg2Rad * camera.fieldOfView);

        var cameraTransform = camera.transform;
        cameraTransform.position = new Vector3(0f, 0f, distance);
        cameraTransform.LookAt(targetObject);

        var tilt = new GameObject("Tilt").transform;
        var rig = new GameObject("CameraRig").transform;
        cameraTransform.SetParent(tilt);
        tilt.SetParent(rig);

        tilt.Rotate(-30f, 0f, 0f);

        return rig;
    }

    private Camera CreateCamera()
    {
        var go = new GameObject("Camera");
        var camera = go.AddComponent<Camera>();
        return camera;
    }

    private void ImageReady(byte[] png)
    {
        if (!string.IsNullOrEmpty(outputMask))
        {
            File.WriteAllBytes(string.Format(outputMask, currentStep), png);
        }
    }

    private Material CreateMaterial()
    {
        var c1 = UnityEngine.Random.ColorHSV();
        var c2 = UnityEngine.Random.ColorHSV();

        var texture = TextureHelper.CreateGradientTexture(128, 1, Color.white, Color.white, c1, c2);

        var material = new Material(shader);

        material.mainTexture = texture;

        return material;
    }
}
