﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class MessageBox : MonoBehaviour
{
    [SerializeField]
    private Text caption;

    private Action onAccept;
    private Action onCancel;

    [SerializeField]
    private Button okButton;

    [SerializeField]
    private Button cancelButton;


    private void Awake()
    {
        Hide();
    }

    public void Show(string caption, Action onAccept, Action onCancel)
    {
        this.onAccept = onAccept;
        this.onCancel = onCancel;
        this.caption.text = caption;

        okButton.gameObject.SetActive(onAccept != null);
        cancelButton.gameObject.SetActive(onCancel != null);

        Show();
    }

    private void Show()
    {
        gameObject.SetActive(true);
    }

    private void Hide()
    {
        gameObject.SetActive(false);
    }

    public void OnOKButtonClicked()
    {
        Hide();
        try
        {
            onAccept?.Invoke();
        }
        catch (Exception e)
        {
            Debug.Log(e);
        }
    }

    public void OnCancelButtonClicked()
    {
        Hide();
        try
        {
            onCancel?.Invoke();
        }
        catch (Exception e)
        {
            Debug.Log(e);
        }
    }
}
